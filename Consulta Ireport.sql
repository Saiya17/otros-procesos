SELECT
provedor.proveedor_nombre as Proveedor,
count (DETALLE_LOTE.LOTE_DETALLE_CANTIDAD) as Cantidad_de_Compras
from provedor
inner join detalle_lote on detalle_lote.lote_codigo = provedor.proveedor_codigo
group BY
Proveedor;