/*Trigger que al actualizar la cantidad de stock en el inventario almacene automáticamente 
  esta información en otra tabla, además, del nombre de usuario que realiza la actualización, 
  fecha y hora de esta.*/


-- PRIMERO CREAR LA TABLA DE AUDITORIAINVENTARIO
create table AUDITORIAINVENTARIO (
   AUDITORIAINVENTARIO_COD      SERIAL         NOT null,
   SUCURSAL_CODIGO      INT4                 NOT null,
   PRODUCTO_CODIGO     INT4                 NOT null,
   INVENTARIO_CANTIDAD_BODEGA INT4                 NOT null,
   INVENTARIO_CANTIDAD_BODEGA_ACTUALIZADO INT4     NOT null,
   USUARIO              VARCHAR(50)                NOT null,
   FECHA                DATE                       NOT null,
   TIEMPO				TIME					   NOT null,
   constraint PK_AUDITORIAINVENTARIO primary key (AUDITORIAINVENTARIO_COD)
);





-- SEGUNDO EJECUTAR EL TRIGGER
CREATE OR REPLACE FUNCTION TR_AUDITORIAINVENTARIO() 
RETURNS TRIGGER AS
$$
DECLARE
BEGIN
	insert into AUDITORIAINVENTARIO(SUCURSAL_CODIGO,PRODUCTO_CODIGO,
									INVENTARIO_CANTIDAD_BODEGA,
									INVENTARIO_CANTIDAD_BODEGA_ACTUALIZADO,
								    USUARIO,FECHA,TIEMPO) 
									VALUES(OLD.SUCURSAL_CODIGO, OLD.PRODUCTOS_CODIGO,
										  OLD.INVENTARIO_CANTIDAD_BODEGA,
										  new.INVENTARIO_CANTIDAD_BODEGA,
										  current_user,
										  current_date,
										  current_time);
  return new;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER TR_AUDITORIAINVENTARIO before update
on inventario for each row
EXECUTE PROCEDURE TR_AUDITORIAINVENTARIO();




--COMPROBAR QUE EXISTAN REGISTROS EN INVENTARIO
select * from INVENTARIO;




--MODIFICAR EL REGISTRO 223 DE LA TABLA INVENTARIO
update INVENTARIO set 
INVENTARIO_CANTIDAD_BODEGA = 100
WHERE INVENTARIO_CODIGO = 222




--COMPROBAR LA TABLA AUDITORIAINVENTARIO
select * from AUDITORIAINVENTARIO;